# ASVR-client

Another [SuteraVR](https://github.com/SuteraVR/SuteraVR) client using [rust](https://www.rust-lang.org/) and [bevyengine](https://bevyengine.org/).
